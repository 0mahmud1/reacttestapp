import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import AddComponent from '../components/add.js';


class AddContainer extends Component {
  render() {
    return (
      <AddComponent />
    );
  }
}

function mapStateToProps(store) {
  return {
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}

// We don't want to return the plain BlogContainer (component) anymore,
// we want to return the smart Container
//  > BlogContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(AddContainer);
