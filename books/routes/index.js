import React from 'react';
import { Route, IndexRoute } from 'react-router';

import BookContainer from '../containers/book.js';
import AddContainer from '../containers/add.js';
import EditContainer from '../containers/edit.js';

export default function () {
  return (
    <Route path='books'>
      <IndexRoute component={BookContainer} />
      <Route path = "add" component = {AddContainer} />
      <Route path = ":id/edit" component = {EditContainer} />
    </Route>
  );
};
