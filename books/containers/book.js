import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import BookComponent from '../components/book.js';
import { getAllBook } from '../actions/getAllBook.js';

class BookContainer extends Component {
  componentWillMount() {
    this.props.getAllBook();
  }
  render() {
    return (
      <BookComponent allBook={this.props.allBook}/>
    );
  }
}

function mapStateToProps(store) {
  return {
    allBook: store.allBook
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getAllBook: getAllBook,
  }, dispatch);
}

// We don't want to return the plain BlogContainer (component) anymore,
// we want to return the smart Container
//  > BlogContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(BookContainer);
