import React, { Component } from 'react';
import { Link } from 'react-router';

class NavbarComponent extends Component {
  render() {
    return (
      <nav className='navbar navbar-default navbar-fixed-top'>
        <div className='container-fluid'>
          <div className='navbar-header'>
            <Link to='/' className='navbar-brand'>
              REACT TEST APP
            </Link>
          </div>
          <ul className='nav navbar-nav'>
            <li><Link to='/books'>Book</Link></li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavbarComponent;
