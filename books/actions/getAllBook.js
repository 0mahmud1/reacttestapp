import axios from 'axios';

export function getAllBook(callback) {
  return function (dispatch) {
    dispatch({
      type: 'FETCH_ALL_BOOK_REQUEST',
    });
    axios.get('books')
      .then((response) => {
        dispatch({
          type: 'FETCH_ALL_BOOK_SUCCESS',
          payload: response.data,
        });
        if (typeof callback === 'function') {
          callback(null, response.data);
        }
      })
      .catch((err) => {
        dispatch({
          type: 'FETCH_ALL_BOOK_FAILURE',
          payload: error.response.data,
        });
        if (typeof callback === 'function') {
          callback(error.response.data);
        }
      });
  };
}
