
export const allBookReducer = (state = [], action) => {
  switch (action.type) {
    case 'FETCH_ALL_BOOK_REQUEST':
      return state;
    case 'FETCH_ALL_BOOK_SUCCESS':
      return action.payload.data;
    case 'FETCH_ALL_BOOK_FAILURE':
      return state;
    default:
      return state;
  }
};
